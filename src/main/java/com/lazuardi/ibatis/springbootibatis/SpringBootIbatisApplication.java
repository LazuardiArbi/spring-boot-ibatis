package com.lazuardi.ibatis.springbootibatis;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.lazuardi.ibatis.springbootibatis.dao.UserDao;
import com.lazuardi.ibatis.springbootibatis.dao.UserDaoIbatis;
import com.lazuardi.ibatis.springbootibatis.dto.UserTeo;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.io.Reader;


@SpringBootApplication
public class SpringBootIbatisApplication {

    public static void main(String[] args) throws IOException {
        //Initialize dao
        UserDao manager = new UserDaoIbatis();

        //Create the SQLMapClient
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient (reader);

        //Create a new user to persist
        UserTeo user = new UserTeo();
        user.setId(1);
        user.setName("Demo User");
        user.setPassword("password");
        user.setEmail("demo-user@howtodoinjava.com");
        user.setStatus(1);

        //Add the user
        manager.addUser(user,sqlmapClient);

        //Fetch the user detail
        UserTeo createdUser = manager.getUserById(1, sqlmapClient);
        System.out.println(createdUser.getEmail());

        //Lets delete the user
//        manager.deleteUserById(1, sqlmapClient);
    }

}
