package com.lazuardi.ibatis.springbootibatis.dao; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.lazuardi.ibatis.springbootibatis.dto.UserTeo;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: UserDao.java, v 0.1 2019-03-12 15:07 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public interface UserDao {
    UserTeo addUser(UserTeo user, SqlMapClient sqlMapClient);
    UserTeo getUserById(Integer id, SqlMapClient sqlMapClient);
    void deleteUserById(Integer id, SqlMapClient sqlMapClient);
}