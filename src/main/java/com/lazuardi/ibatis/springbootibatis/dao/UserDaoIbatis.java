package com.lazuardi.ibatis.springbootibatis.dao; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.lazuardi.ibatis.springbootibatis.dto.UserTeo;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: UserDaoIbatis.java, v 0.1 2019-03-12 15:11 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public class UserDaoIbatis implements UserDao{

    @Override
    public UserTeo addUser(UserTeo user, SqlMapClient sqlMapClient) {
        try
        {
            Integer id = (Integer) sqlMapClient.queryForObject("user.getMaxId");
            id = id == null ? 1 : id + 1;
            user.setId(id);
            user.setStatus(1);
            sqlMapClient.insert("user.addUser", user);
            user = getUserById(id, sqlMapClient);
            return user;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserTeo getUserById(Integer id, SqlMapClient sqlMapClient) {
        try
        {
            UserTeo user = (UserTeo) sqlMapClient.queryForObject("user.getUserById", id);
            return user;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteUserById(Integer id, SqlMapClient sqlMapClient) {
        try
        {
            sqlMapClient.delete("user.deleteUserById", id);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}