package com.lazuardi.ibatis.springbootibatis.dto; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: UserTeo.java, v 0.1 2019-03-12 15:05 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public class UserTeo {
    private Integer id;
    private String name;
    private String email;
    private String password;
    private int status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}